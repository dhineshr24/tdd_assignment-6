import unittest
from palindrome_checking import Word
from read_file import Read

w=Read("input.txt").readFromFile()

class test_palindrome_check(unittest.TestCase):
    def test_empty_string(self):
        word = Word(w[0].strip())
        self.assertEqual(word.palindrome(),True)

    def test_single_letter(self):
        word = Word(w[1].strip())
        self.assertEqual(word.palindrome(), True)

    def test_double_letter(self):
        word = Word(w[2].strip())
        self.assertEqual(word.palindrome(), True)

    def test_odd_length_string(self):
        word = Word(w[3].strip())
        self.assertEqual(word.palindrome(), True)

    def test_even_length_string(self):
        word = Word(w[4].strip())
        self.assertEqual(word.palindrome(), True)

if __name__ == '__main__':
    unittest.main()

