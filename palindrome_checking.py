from dataclasses import dataclass

@dataclass
class Word(object):
      string:str
      def palindrome(self):
          self.string = self.string.lower();
          flag=True
          for i in range(0, len(self.string)//2):
            if(self.string[i] != self.string[len(self.string)-i-1]):
                flag = False;
                break;
          return flag
